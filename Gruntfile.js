
module.exports = function(grunt) {

  grunt.config.set('custom', {
    root: __dirname,
    dev_dirdeploy: 'build/',
    stag_dirdeploy: 'build/',

    option: function(name) {
      try {
        return grunt.option(name) || this[name];
      }
      catch(e) {
        return false;
      }
    }
  });

  grunt.loadTasks('./tasks');
};
