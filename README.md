# JSkel
Hecho con amor para tener tiempo para el facebook.

- Configuración de tareas con [Gruntjs](http://gruntjs.com).
- Hecho para funcionar con [nodeJS](http://nodejs.org/), [jade](http://jade-lang.com/), [stylus](http://learnboost.github.io/stylus/), [requirejs](http://requirejs.org/), [glue](https://glue.readthedocs.org/en/latest/), [connect](http://www.senchalabs.org/connect/), etc. 
-------------------------------------------------------------------------------------

## Requisitos

- [NodeJS](http://nodejs.org/) >= 0.10 
- [Grunt CLI](http://gruntjs.com/getting-started)
- [Python](https://www.python.org/downloads/) >= 2.7 _(para usar glue)_
- [Glue](http://glue.readthedocs.org/en/latest/installation.html)


-------------------------------------------------------------------------------------

## Agradecimientos solamente. ¡Nada de dinero!
- [Luis Miguel](https://github.com/zrobit)
- [Elizabeth](http://www.emanrique.com)
