/**
 * Publico
 */
module.exports = function(grunt) {
  var dest;
  var config = grunt.config.get('custom');

  // cargo dependencias
  grunt.loadNpmTasks('grunt-contrib-copy');

  // condiciono destino del build
  if (config.option('prod')) {
    dest = config.stag_dirdeploy;
  }

  else {
    dest = config.dev_dirdeploy;
  }

  // configuro tarea que copiara archivos libres de pre-procesos
  grunt.config.set('copy', {
    compile: {
      files: [
        { expand: true,
          src: [
            'static/images/**',
            '!**/*.styl'
          ],
          dest: dest
        }
      ]
    },

    js: {
      files: [
        { expand: true,
          src: [
            'static/scripts/**'
          ],
          dest: dest
        }
      ]
    }
  });

  // configuro tarea
  grunt.registerTask('deploy', 'Compile project', function() {
    grunt.task.run(['sprites', 'stylus', 'copy:compile', 'jade']);

    if (config.option('prod')) {
      grunt.task.run(['requirejs']);
    }

    else {
      grunt.task.run(['copy:js']);
    }
  });

};
