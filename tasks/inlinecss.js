var fs = require('fs');
var request = require('request');
var cheerio = require('cheerio');
var querystring = require('querystring');

function compileHTMLCSSINLINENOW(opt, call) {
  opt = opt || {};

  var html = fs.readFileSync(
    opt['in'], 
    { encoding: 'utf-8' }
  );

  var post_data = querystring.stringify({
    html: html,
    'strip-css': 'on'
  });


  request.post({
    headers: {'content-type' : 'application/x-www-form-urlencoded'},
    url: 'http://templates.mailchimp.com/resources/inline-css/',
    body: post_data
  }, function(err, resp, body) {
    var $ = cheerio.load(body);
    var compile = $('.form-area').eq(1).val();
    fs.writeFileSync(opt.out, compile, {
      encoding: 'utf-8'
    });
    if (typeof call === 'function') {
      call();
    }
  });
}

module.exports = function(grunt) {
  var dest;
  var config = grunt.config.get('custom');

  // condiciono destino del build
  if (config.option('prod')) {
    dest = config.stag_dirdeploy;
  }

  else {
    dest = config.dev_dirdeploy;
  }

  // configuro tarea
  grunt.registerTask('inlinecss', 'Compile css inline', function() {
    var done = this.async();

    compileHTMLCSSINLINENOW({
      'in': dest + '/mail.html',
      'out': dest + '/mail-listo.html'
    });

  });
};
