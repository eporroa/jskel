
/**
 * Publico
 */
module.exports = function(grunt) {
  var dest;
  var config = grunt.config.get('custom');
  var handler = require(config.root + '/handler.js');
  var options = {
    pretty: true
  };

  // cargo dependencias
  grunt.loadNpmTasks('grunt-contrib-jade');

  // condiciono destino del build
  if (config.option('prod')) {
    dest = config.stag_dirdeploy + '/templates';
    options.pretty = false;
  }

  else {
    dest = config.dev_dirdeploy;
    options.pretty = true;
  }

  options.data = function(DEST) {
    handler.FILE_DEST = DEST;
    handler.DEST = dest;

    return {
      handler: handler,
      grunt: config
    };
  };

  // configuro tarea
  grunt.config.set('jade', {
    compile: {
      options: options,
      files: [
        {
          expand: true,

          // TODO: revisar watch, "templates/" se suprime del path
          // cuando se quiere compilar un solo archivo
          cwd: 'templates/',

          src: [
            '*.jade',
            '**/*.jade',
            '!_*',
            '!**/_*'
          ],
          dest: dest,
          ext: '.html'
        }
      ]
    }
  });

};
