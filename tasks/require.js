/**
 * Publico
 */
module.exports = function(grunt) {
  var dest;
  var config = grunt.config.get('custom');

  // cargo dependencias
  grunt.loadNpmTasks('grunt-requirejs');

  // condiciono destino del build
  if (config.option('prod')) {
    dest = config.stag_dirdeploy + '/static/scripts';
  }

  else {
    dest = config.dev_dirdeploy + '/static/scripts';
  }

  // configuro tarea
  grunt.config.set('requirejs', {
    compile: {
      options: {
        baseUrl: 'static/scripts',
        dir: dest,
        preserveLicenseComments: false,
        almond: true,

        paths: {},

        modules: [
          { name: 'site.index' }
        ]
      }
    }
  });
};
