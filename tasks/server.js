// variables globales
var connect = require('connect');
var grunt;

/**
 * staticServer
 * @param host string Direccion IP del server, por defecto localhost
 * @param port number Puerto del server, por defecto 8000
 */
function staticServer(host, port) {
    var done = this.async();
    var config = grunt.config.get('custom');
    var dest;

    if (config.option('prod')) {
      dest = config.stag_dirdeploy;
    }

    else {
      dest = config.dev_dirdeploy;
    }

    host = host || '0.0.0.0';
    port = port || 8000;

    connect()

    .use('/static/scripts', connect.static(
      config.root + '/static/scripts')
    )

    .use('/static/styles', connect.static(
      dest + '/static/styles')
    )

    .use('/static', connect.static(
      dest + '/static')
    )

    .use('/', connect.static(dest))

    .listen(port, host);

    grunt.log.write('\nStarting static web server in "%s" on port %s.', host, port);
}

/**
 * Publico
 */
module.exports = function(g) {
  grunt = g;

  grunt.registerTask('connect', 'Start a custom static webserver', staticServer);

};
