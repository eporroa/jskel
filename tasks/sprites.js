/**
 * Publico
 */
module.exports = function(grunt) {
  var dest;
  var config = grunt.config.get('custom');

  // cargo dependencias
  grunt.loadNpmTasks('grunt-glue');

  // condiciono destino del build
  if (config.option('prod')) {
    dest = config.stag_dirdeploy + '/static/sprites/';
  }

  else {
    dest = config.dev_dirdeploy + '/static/sprites/';
  }

  // configuro tarea
  grunt.registerTask('sprites', 'Compile sprites', function() {
    var items = grunt.file.expand({}, [
          'static/sprites/*', '!static/sprites/**.*'
        ]),
        i = items.length - 1,
        sprites = {};

    for (i; i > -1; --i) {
      sprites['sprite' + i] = {
        src: [ items[i] ],
        options: '--css=' + dest +
          ' --img=' + dest +
          ' --cachebuster --namespace=sp --url=../sprites/ --margin=10 --imagemagick'
      };
    }

    grunt.config.set('glue', sprites);

    grunt.config.set('stylus.glue', {
      options: {
        '-C': true
      },
      files: [
        {
          expand: true,
          cwd: dest,
          src: [
            '*.css'
          ],
          dest: 'static/sprites',
          ext: '.styl'
        }
      ]
    });

    grunt.task.run('glue');
    grunt.task.run('stylus:glue');
  });
};
