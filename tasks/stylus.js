/**
 * Publico
 */
module.exports = function(grunt) {
  var dest;
  var config = grunt.config.get('custom');

  // cargo dependencias
  grunt.loadNpmTasks('grunt-contrib-stylus');

  // condiciono destino del build
  if (config.option('prod')) {
    dest = config.stag_dirdeploy + '/static';
  }

  else {
    dest = config.dev_dirdeploy + '/static';
  }


  // configuro tarea
  grunt.config.set('stylus', {
    compile: {
      options: {
        compress: true
      },
      files: [
        {
          expand: true,

          // TODO: revisar watch, "static" se suprime del path
          // cuando se quiere compilar un solo archivo
          cwd: 'static',

          src: [
            '**.styl',
            '**/**.styl',
            '!**/modules/**'
          ],
          dest: dest,
          ext: '.css'
        }
      ]
    }
  });

};
